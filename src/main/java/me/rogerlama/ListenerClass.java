package me.rogerlama;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import com.mojang.authlib.GameProfile;
import java.util.UUID;

import org.bukkit.GameMode;
import org.bukkit.Material;

public class ListenerClass implements Listener {
		
	public ListenerClass() {
	}
	
	@EventHandler
	public void onBreak(BlockBreakEvent event) {
		
		if(event.getPlayer().getGameMode() != GameMode.SURVIVAL || !event.getPlayer().hasPermission("customdrops.drops")) {
			return;		
		}
		
		String dropChance = "0";
		double doubleDropChance = 0;
		double dropChanceRoll = 1;
		Material Toolused = null;
		Material blockToBreak = null;
		String Headurl = null;
		String id = null;
		String item = null;
		GameProfile profile = null;
		
		Player player = event.getPlayer();
		Block block = event.getBlock(); //Block broken
		Material material = block.getType(); //Block material

		item = Item.getItem(material);
		
		if((material == Material.matchMaterial(MainClass.self.getConfig().getString(item + ".Block"))) && !(item.equals("Default"))) {
			
			if(MainClass.self.getConfig().getString(item + ".HeadUrl") != null) {
				Headurl = MainClass.self.getConfig().getString(item + ".HeadUrl");
			}
			
			if(MainClass.self.getConfig().getString(item + ".Block") != "NONE") {
				blockToBreak = Material.matchMaterial(MainClass.self.getConfig().getString(item + ".Block"));
			}
			
			if(MainClass.self.getConfig().getString(item + ".Tool") != "NONE") {
				Toolused = Material.matchMaterial(MainClass.self.getConfig().getString(item + ".Tool"));
			}
			
			dropChance = MainClass.self.getConfig().getString(item + ".Rate");
			doubleDropChance = Double.parseDouble(dropChance);
			
			if(Headurl != null) {
				id = MainClass.self.getConfig().getString(item + ".UUID");
				profile = new GameProfile(UUID.fromString(id), null);
			}
		}
		
		if(((material == blockToBreak) && ((Toolused == player.getInventory().getItemInMainHand().getType()) || (Toolused == null))) && !(item.equals("Default"))) {
			dropChanceRoll = Math.random() * 100;
			if(dropChanceRoll <= doubleDropChance) {
				if(Headurl != null) {
					block.getWorld().dropItemNaturally(block.getLocation(), Item.getCustomSkull(Headurl, profile, item));
				} else {
					block.getWorld().dropItemNaturally(block.getLocation(), Item.getCustomItem(item));
				}
			}
		}
	}
}