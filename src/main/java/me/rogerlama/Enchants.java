package me.rogerlama;

import org.bukkit.enchantments.Enchantment;

public class Enchants {
	
	public static Enchantment getEnchant(int i, String[] enchantList) {
    	Enchantment enchant = null;
    	String currentEnchant = enchantList[i];
    	
		if(currentEnchant.equals("Power")){
    		enchant = Enchantment.ARROW_DAMAGE;
    	}
		else if(currentEnchant.equals("Flame")) {
			enchant = Enchantment.ARROW_FIRE;
		}
		else if(currentEnchant.equals("Infinity")) {
			enchant = Enchantment.ARROW_INFINITE;
		}
		else if(currentEnchant.equals("Punch")) {
			enchant = Enchantment.ARROW_KNOCKBACK;
		}
		else if(currentEnchant.equals("Curse of Binding")) {
			enchant = Enchantment.BINDING_CURSE;
		}
		else if(currentEnchant.equals("Sharpness")) {
			enchant = Enchantment.DAMAGE_ALL;
		}
		else if(currentEnchant.equals("Bane of Arthropods")) {
			enchant = Enchantment.DAMAGE_ARTHROPODS;
		}
		else if(currentEnchant.equals("Smite")) {
			enchant = Enchantment.DAMAGE_UNDEAD;
		}
		else if(currentEnchant.equals("Depth Strider")) {
			enchant = Enchantment.DEPTH_STRIDER;
		}
		else if(currentEnchant.equals("Efficiency")) {
			enchant = Enchantment.DIG_SPEED;
		}
		else if(currentEnchant.equals("Unbreaking")) {
			enchant = Enchantment.DURABILITY;
		}
		else if(currentEnchant.equals("Fire Aspect")) {
			enchant = Enchantment.FIRE_ASPECT;
		}
		else if(currentEnchant.equals("Frost Walker")) {
			enchant = Enchantment.FROST_WALKER;
		}
		else if(currentEnchant.equals("Knockback")) {
			enchant = Enchantment.KNOCKBACK;
		}
		else if(currentEnchant.equals("Fortune")) {
			enchant = Enchantment.LOOT_BONUS_BLOCKS;
		}
		else if(currentEnchant.equals("Looting")) {
			enchant = Enchantment.LOOT_BONUS_MOBS;
		}
		else if(currentEnchant.equals("Luck of the Sea")) {
			enchant = Enchantment.LUCK;
		}
		else if(currentEnchant.equals("Lure")) {
			enchant = Enchantment.LURE;
		}
		else if(currentEnchant.equals("Mending")) {
			enchant = Enchantment.MENDING;
		}
		else if(currentEnchant.equals("Respiration")) {
			enchant = Enchantment.OXYGEN;
		}
		else if(currentEnchant.equals("Protection")) {
			enchant = Enchantment.PROTECTION_ENVIRONMENTAL;
		}
		else if(currentEnchant.equals("Blast Protection")) {
			enchant = Enchantment.PROTECTION_EXPLOSIONS;
		}
		else if(currentEnchant.equals("Feather Falling")) {
			enchant = Enchantment.PROTECTION_FALL;
		}
		else if(currentEnchant.equals("Fire Protection")) {
			enchant = Enchantment.PROTECTION_FIRE;
		}
		else if(currentEnchant.equals("Projectile Protection")) {
			enchant = Enchantment.PROTECTION_PROJECTILE;
		}
		else if(currentEnchant.equals("Silk Touch")) {
			enchant = Enchantment.SILK_TOUCH;
		}
		else if(currentEnchant.equals("Sweeping Edge")) {
			enchant = Enchantment.SWEEPING_EDGE;
		}
		else if(currentEnchant.equals("Thorns")) {
			enchant = Enchantment.THORNS;
		}
		else if(currentEnchant.equals("Curse of Vanishing")) {
			enchant = Enchantment.VANISHING_CURSE;
		}
		else if(currentEnchant.equals("Aqua Affinity")) {
			enchant = Enchantment.WATER_WORKER;
		} else {
			System.out.println("Invalid Enchant name in config.yml");
		}
		
    	return enchant;
}
    
    public static int getEnchantLevel(int i, String[] enchantValList) {
    	return Integer.parseInt(enchantValList[i]);
    }
}