package me.rogerlama;

import java.util.Arrays;

import org.apache.commons.codec.binary.Base64;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import com.mojang.authlib.properties.PropertyMap;

public class Item {

    public static ItemStack getCustomSkull(String url, GameProfile profile, String headname) {
    	final Base64 base64 = new Base64();
        PropertyMap propertyMap = profile.getProperties();
        if (propertyMap == null) {
            throw new IllegalStateException("Profile doesn't contain a property map");
        }
        byte[] encodedData = base64.encode(String.format("{textures:{SKIN:{url:\"%s\"}}}", url).getBytes());
        propertyMap.put("textures", new Property("textures", new String(encodedData)));
        ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
        ItemMeta headMeta = head.getItemMeta();
        Class<?> headMetaClass = headMeta.getClass();
        Reflections.getField(headMetaClass, "profile", GameProfile.class).set(headMeta, profile);
        
        //Name
        String name = MainClass.self.getConfig().getString(headname + ".Name");
        headMeta.setDisplayName(replaceColors(name));
        
        //ArrayList<String> lore = new ArrayList<String>();
        //lore.add(MainClass.self.getConfig().getString(headname + ".Lore"));
        
        //Lore
        if(MainClass.self.getConfig().getString(headname + ".Lore") != null) {
        	String lore = ChatColor.DARK_GRAY + MainClass.self.getConfig().getString(headname + ".Lore");
        	headMeta.setLore(Arrays.asList(replaceColors(lore)));
        }
        
        head.setItemMeta(headMeta);

        return head;
    }
    
    public static ItemStack getCustomItem(String pathToItem) {
    	ItemStack item = new ItemStack(Material.matchMaterial(MainClass.self.getConfig().getString(pathToItem + ".Item")));
    	ItemMeta itemMeta = item.getItemMeta();
    	
    	//Name
    	String name = MainClass.self.getConfig().getString(pathToItem + ".Name");
    	itemMeta.setDisplayName(replaceColors(name));
    	
    	//Lore
    	if(MainClass.self.getConfig().getString(pathToItem + ".Lore") != null) {
    		String lore = ChatColor.DARK_GRAY + MainClass.self.getConfig().getString(pathToItem + ".Lore");
        	itemMeta.setLore(Arrays.asList(replaceColors(lore)));
        }
    	
    	//Enchants
    	if(MainClass.self.getConfig().getString(pathToItem + ".Enchants") != null) {
	    	String enchantsStr = MainClass.self.getConfig().getString(pathToItem + ".Enchants");
	    	String[] enchantList = enchantsStr.split(", ");
	    	String enchantsValStr = MainClass.self.getConfig().getString(pathToItem + ".Enchant Values");
	    	String[] enchantValList = enchantsValStr.split(", ");
	    	
	    	for(int i = 0; i < enchantList.length; i++) {
				itemMeta.addEnchant(Enchants.getEnchant(i, enchantList), Enchants.getEnchantLevel(i, enchantValList),
						true);
	    	}
    	}
    	
    	//Glow
    	if(MainClass.self.getConfig().getBoolean(pathToItem + ".Glow") && MainClass.self.getConfig().getString(pathToItem + ".Enchants") == null) {
    		itemMeta.addEnchant(Enchantment.LURE, 0, true);
            itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
		}
    	
    	item.setItemMeta(itemMeta);

    	return item;
    }
    
    public static String getItem(Material block) {
		
		String item = "Default";
		String[] itemList = null;
		
		for(String key : MainClass.self.getConfig().getKeys(false)){
			itemList = key.split("\n");
			for (int i = 0; i < itemList.length; i++) {
				if (block == Material.matchMaterial(MainClass.self.getConfig().getString(itemList[i] + ".Block"))) {
					item = itemList[i];
				}
			}
		} 

		return item;
	}
    
    public static String replaceColors(String text) {
    	
    	text = text.replaceAll("&0", ChatColor.BLACK + "");
        text = text.replaceAll("&1", ChatColor.DARK_BLUE + "");
        text = text.replaceAll("&2", ChatColor.DARK_GREEN + "");
        text = text.replaceAll("&3", ChatColor.DARK_AQUA + "");
        text = text.replaceAll("&4", ChatColor.DARK_RED + "");
        text = text.replaceAll("&5", ChatColor.DARK_PURPLE + "");
        text = text.replaceAll("&6", ChatColor.GOLD + "");
        text = text.replaceAll("&7", ChatColor.GRAY + "");
        text = text.replaceAll("&8", ChatColor.DARK_GRAY+ "");
        text = text.replaceAll("&9", ChatColor.BLUE + "");
        text = text.replaceAll("&A", ChatColor.GREEN + "");
        text = text.replaceAll("&B", ChatColor.AQUA + "");
        text = text.replaceAll("&C", ChatColor.RED + "");
        text = text.replaceAll("&D", ChatColor.LIGHT_PURPLE + "");
        text = text.replaceAll("&E", ChatColor.YELLOW + "");
        text = text.replaceAll("&F", ChatColor.WHITE + "");
        text = text.replaceAll("&M", ChatColor.STRIKETHROUGH + "");
        text = text.replaceAll("&N", ChatColor.UNDERLINE + "");
        text = text.replaceAll("&L", ChatColor.BOLD + "");
        text = text.replaceAll("&K", ChatColor.MAGIC + "");
        text = text.replaceAll("&O", ChatColor.ITALIC + "");
    	
		return text;
    }
}