package me.rogerlama;

import java.io.File;

import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

public class MainClass extends JavaPlugin {

	public static MainClass self;

	@Override
	public void onEnable() {
		self = this;
		this.getServer().getPluginManager().registerEvents(new ListenerClass(), this);
		this.loadConfig();
		// Register Commands
		DropsCommandHandler commandHandler = new DropsCommandHandler();
		this.getCommand("customdrops").setExecutor(commandHandler);
	}
	
	@Override
	public void onDisable() {
		HandlerList.unregisterAll(this);
		self = null;
	}

	/**
	 * Reload the configuration.
	 */
	public void reload() {
		this.reloadConfig();
	}

	/*
	 * Loads the configuration and saves it out to the directory if it was not all
	 * ready there.
	 */
	protected boolean loadConfig() {
		try {
			// make sure data folder is there
			if (!this.getDataFolder().exists()) {
				this.getDataFolder().mkdirs();
			}

			File cfgFile = new File(getDataFolder(), "config.yml");
			if (!cfgFile.exists()) {
				this.saveDefaultConfig();
			}
			this.getConfig().load(cfgFile);
			return true;
		} catch (Exception e) {
			this.getLogger().severe("Failed to load configuration! Please correct the error and use /reload to fix. :: "
					+ e.getMessage());
		}
		return false;
	}
}
