package me.rogerlama;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class DropsCommandHandler implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length == 0) {
			return false;
		}
		if ("reload".equals(args[0])) {
			if (sender.hasPermission("customdrops.admin")) {
				MainClass.self.reload();
				sender.sendMessage("CustomDrops configuration reloaded.");
				return true;
			} else {
				sender.sendMessage("You do not have the customdrops.admin permission!");
				return true;
			}
		}
		return false;
	}

}
